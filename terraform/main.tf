#Following environment variables must be exported prior to running
#  AWS_ACCESS_KEY_ID
#  AWS_SECRET_ACCESS_KEY
provider "aws" {
    region           = "eu-central-1"
}

resource "aws_instance" "vps" {
    count            = 3
    ami              = "ami-07df274a488ca9195"
    instance_type    = "t2.medium"
    key_name         = "FXGames"
    vpc_security_group_ids = [aws_security_group.SGFX.id]
    tags             = {
        Name    = "FX Games dev ${count.index}"
        Stack   = "Development"
        Purpose = "NGINX"
    }
}

resource "aws_security_group" "SGFX" {
  name = "FXGames Security Group"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

