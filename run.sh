#!/bin/env bash
############################################
#) Create a user with full access
#) Create key-pair called "FXGAMES", put .pem file in the directory along with this script
#) Export variables:
#  + AWS_ACCESS_KEY_ID
#  + AWS_SECRET_ACCESS_KEY
#) Enable aws_ec2 inventory module in Ansible configuration file
#############################################
pip install --user boto3
cd terraform
terraform init
yes yes | terraform apply
cd ../ansible
ansible-playbook -i inventory_aws_ec2.yml playbook.yml
cd ..
